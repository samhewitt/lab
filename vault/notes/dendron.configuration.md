---
id: lgg4qs09ku5lqp6egxvfadb
title: Configuration
desc: ''
updated: 1689549884536
created: 1686621514363
---
the default `dendron.yml` file specifies task keywords as [described by the Dendron team](https://wiki.dendron.so/notes/3h1jfpuz3amtju1tzfvb7vr/):


```yaml
    task:
        name: task
        dateFormat: y.MM.dd
        addBehavior: asOwnDomain
        statusSymbols:
            '': ' '
            wip: w
            done: x
            assigned: a
            moved: m
            blocked: b
            delegated: l
            dropped: d
            pending: 'y'
```

In my vault, this is [lab/vault/dendron.yml](..//dendron.yml)