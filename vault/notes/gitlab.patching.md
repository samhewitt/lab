---
id: qrgs7lckhkjtch76sjlwfze
title: Patching
desc: ''
updated: 1688007197525
created: 1688007134360
tags:
  - work
  - gitlab
  - howto
---

This is a great example of how to write patching instructions:

https://gitlab.com/-/snippets/2563676

I like how it is self-contained, and has clear instructions for applying and reversing, with the expected outputs
