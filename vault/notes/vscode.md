---
id: ejtn4hgon63ypue26j0pyr4
title: VSCode
desc: ''
updated: 1686697699015
created: 1686566183089
---

[Visual Studio Code](https://code.visualstudio.com) (VSCode, or "_viscode_") is a file editing environment for computer engineers. It is primarily an extensible, programmable text editor, and is both less _and more than_ an "integrated development environment".

Indeed, I have adopted VSCode as [my main computing environment](https://milosophical.me/blog/2021/fave-editor-2020s.html), replacing EMACS which used to have that role.

While settings and usage are [well documented](https://code.visualstudio.com/docs), I would also like a place to keep notes about my usage of VSCode for work related tasks:

- What settings have I applied?
- Which extensions have I added?
- Why have I done so? Pros and Cons? For what contexts?
- A place to note special key bindings

This hierarchy of notes is for these kinds of information, both as reference, and potentially to share with others.