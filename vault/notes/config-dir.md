---
id: e3jwvrx2cxd1ismwgu1ni09
title: Config Dir
desc: ''
updated: 1687907132402
created: 1687906950373
---

Application settings are stored in a user-config dir. This location is dependent on the operating system

## [[macOS]]

`$HOME/Library/Application Support/`

## [[Linux]]

`$XDG_CONFIG`, which is set to `$HOME/.config` by default

## [[Windows-NT]]

`%APP_DIR%`, which is set to `%USERDIR%/AppData/Roaming` on most PCs
