[CDP](https://gitlab.com/mlockhart/lab/-/issues/42) | [Support training issues](https://gitlab.com/gitlab-com/support/support-training/-/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username[]=mlockhart) | [FY22Q4 TA](https://docs.google.com/spreadsheets/d/1ZQMtR4bvAhcFePp1ynEDCsaBn-v9IY7fAlo-MDEQNi4/edit#gid=395345394) | [CDP Dump Doc](https://docs.google.com/document/d/1O7ZmxkkQ3hroKRmRM1DsqXqHodGTHz_HviFAuWLCfA4/edit) | [FY22Q1 Goals](https://docs.google.com/document/d/1fQ_iGDqyYRps_615bDinBWNC2UgIYu328-ymaboAmPw/edit#bookmark=id.st5xen39e4z2) | [Quad List](https://gitlab.com/mlockhart/lab/-/boards/2903287) | [2021 360](https://gitlab.cultureamp.com/effectiveness/surveys/60d12446afb34f002b69aeb4/processes/61048a82ae6511868581bbaf/feedback_review/feedback) | [Training wish-list](https://gitlab.com/mlockhart/lab/-/issues/88) | [To Do](https://gitlab.com/mlockhart/lab/-/boards/3834181) | [ZD Metrics dash](https://gitlab.zendesk.com/explore/dashboard/9B66E91E91A208F5AE1EB4381DD4B9C92A4BE4365851456B258295935441FB9B/tab/22432032) | [Conceit Sheet](https://docs.google.com/document/d/1O7ZmxkkQ3hroKRmRM1DsqXqHodGTHz_HviFAuWLCfA4/edit#) | [Clocklify](https://app.clockify.me/tracker)
## ✬ SMART goals for 2022

<details><summary>🌟 Click to expand</summary>

| What | How | When | Status |
|------|-----|------|--------|

  <details><summary>✅ Completed</summary>

| What | When | Status |
|------|------|--------|

  </details>
</details>


## 🎯 This Week's Focus

Try to keep the list to maximum three!

- 🎫 
- 

<details><summary>Daily action prompts</summary>

- :exclamation: Plan 3 Big Things
- :fast_forward: _Slow down_ to go Faster
- :construction_worker: Get on with the doing &rarr; smallest next step
- :question: [Ask for help](https://about.gitlab.com/handbook/support/workflows/how-to-get-help.html) after 45 minutes
- :x: Close all browser windows

</details>

<details><summary>More constructive break ideas</summary>

Instead of YouTube, try these ideas for short breaks

- :frame_photo: Doodling
- :pencil: Write down thoughts (work/tech blog, or journal)
- :coffee: Make a cup of tea/coffee, but then consume it mindfully
- :speaking_head: Talk to someone
- :heart:  Help someone
- :telephone_receiver: Call a friend
- :gift_heart: : Small act of kindness / plan something nice

</details>
⏰ Reminder: Are you on duty?

## ✅ To Do

### :sunny:  Monday

<details><summary>Click to expand</summary>

- [ ] Review auto-generated Weekly Log (this log)
  - Move "Coming up next week" to this week
  - Review booked pairings ...
- [ ] Week-end hangovers
- [ ] Collaboration

</details>

### ⚡️ Higher Priority

<!-- Recurring tasks:
After 18th of the month - [ ] Submit Expense
- [ ] :moneybag: Submit Expense After 18th of the month
-->

- [ ]

### ✨ Lower Priority

- [ ]

### :checkered_flag: Friday

<details><summary>Click to expand</summary>

- [ ] Check [SWIR](https://gitlab.com/gitlab-com/support/readiness/support-week-in-review/-/milestones?state=opened)
- [ ] (optionally) Record [SWIR podcast](https://about.gitlab.com/handbook/support/workflows/how-to-WIR-podcast.html)
- [ ] Arrange at least 2 pairings for next week
- [ ] Tick off at least 1 box in current training
- [ ] Review and close out [todos](https://gitlab.com/dashboard/todos)

</details>

## 🕐 Time Spent This Week

:notepad_spiral: remember to follow these links and look at the collections

- [ ] 🎫 [Tickets](https://gitlab.zendesk.com/agent/users/370095384640/assigned_tickets) [FRT-assigned](https://gitlab.zendesk.com/agent/search/1?type=ticket&q=assignee%3Ame%20order_by%3Acreated_at%20sort%3Adesc%20created_at%3E2022-03-25)
- [ ] 🍄 [Projects](https://gitlab.com/dashboard/issues?scope=all&state=opened&assignee_username=mlockhart&label_name[]=dev)
- [ ] 👟 [Training](https://gitlab.com/dashboard/issues?scope=all&state=opened&assignee_username=mlockhart&my_reaction_emoji=athletic_shoe)
- [ ] 🧪 [Experiments](https://gitlab.com/mlockhart/lab/-/boards/1931655?label_name[]=log%3A%3Aexperiment)
- [ ] :star: [Other Issues](https://gitlab.com/dashboard/issues?scope=all&state=opened&assignee_username=mlockhart&not[label_name][]=fieldnote&not[label_name][]=log&not[label_name][]=dev&not[my_reaction_emoji]=athletic_shoe)
- [ ] 🍐 [Pairing](https://gitlab.com/gitlab-com/support/support-pairing/-/issues?scope=all&state=all&assignee_username[]=mlockhart)

### 🤝 Meetings

<details><summary>Click to expand</summary>

Jot notes from coffee chats and other casual meetings here. Pairings, team meetings, customer calls all have their own place to be (pairing issues, gdocs, fieldnotes).

- 


</details>

## 💭 Weekly Retrospective

### 🏆 Wins

Try to relate to a GitLab value and add to the Wins list at the end of the week, add the relevant labels to the issue.

-

### 📚 What I Learnt

Check ~"TIL: Today I Learned 📚" [entries](https://gitlab.com/mlockhart/lab/-/issues?scope=all&state=all&label_name[]=TIL%3A%20Today%20I%20Learned%20%F0%9F%93%9A) for this week

-

### ❌ What did not go well? Why?

-

## 📆 Coming up, Next Week

- 

---

/label ~"Weekly Log 📝" ~log ~recurring::weekly

/confidential
/assign me
