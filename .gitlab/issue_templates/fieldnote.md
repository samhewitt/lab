<!--
Title:
ZD TICKET_NUM / TICKET_TITLE 

Note: You need to _manually_ change TICKET_NUM, ISSUE_NUM, and TICKET_TITLE, sorry
I would like to write something that creates Issues and updates with link comments etc. 

-->


## Ticket

 * **Zen**: [TICKET_NUM](https://gitlab.zendesk.com/agent/tickets/TICKET_NUM)

<!-- 
 * Drop this into the Zendesk ticket as a Comment: 

```
This Support Ticket also has Field Notes:

https://gitlab.com/mlockhart/lab/-/issues/ISSUE_NUM

(a Fieldnote is a warts-and-all record of investigations for a ticket)
```
-->

## Ticket Summary

 * updated continuosly in this issue's description

## To investigate

 - [ ] use threaded comments for
 - [ ] different investigation paths

## Problem Statement:

Insert Problem Statement Here

## Resolution

TBC


## Relevant Resources

Insert Relevant Resources Here (e.g. other GitLab Issues or MRs, documentation links)

### GitLab Issues:


---

>>>
 #### This Issue is a Field Note

 [Fieldnotes](https://gitlab.com/mlockhart/lab/-/wikis/Fieldnotes) are an out-of-band deep record of investigations for a ticket, and contain
 
  * a Summary to Efficiently bring peers up-to-speed on the work in hand
  * current direction of investigations
  * a template and place to collect a Closing Summary.
>>>

/assign me

/label ~fieldnote ~"fn::open"
/confidential
