# Here is Mike's GitLabBook

This Project keeps personal [work logs](https://gitlab.com/mlockhart/lab/-/issues?label_name%5B%5D=log&state=all), [experiment notes](https://gitlab.com/mlockhart/lab/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=log%3A%3Aexperiment), [code](https://gitlab.com/mlockhart/lab/-/tree/master), [reference notes](https://gitlab.com/mlockhart/lab/-/wikis/pages), [Issues](https://gitlab.com/mlockhart/lab/-/boards), [Snippets](https://gitlab.com/mlockhart/lab/snippets), and [Todos](https://gitlab.com/dashboard/todos?state=&project_id=19846551), which are not part of other Projects in GitLab. It's an ***open lab book*** which anyone may refer to, create Issues in, Clone/Fork or update with a Merge Request.

* [Issues](https://gitlab.com/mlockhart/lab/-/boards) track tasks that have no other Project home
* [My log issues board](https://gitlab.com/mlockhart/lab/-/boards/1894499?label_name[]=log) is an overview of personal, experimental, and weekly logs (the weeklies are confidential)
* The [Wiki](https://gitlab.com/mlockhart/lab/-/wikis/home) is for keeping short [reference notes](https://gitlab.com/mlockhart/lab/-/wikis/pages), and meta-notes about this Project
* The [Repository](https://gitlab.com/mlockhart/lab/-/tree/master) keeps code for tools and experiments, some may also merge into other Groups' Projects
* [Snippets](https://gitlab.com/mlockhart/lab/snippets) should be generally applicable to my roles at GitLab. Again, some may also merge elsewhere
* You can also find [occasional logs about my job](https://gitlab.com/mlockhart/lab/-/issues?label_name%5B%5D=log&state=all), what is [going on personally](https://gitlab.com/mlockhart/lab/-/issues?label_name%5B%5D=log%3A%3Apersonal&state=all), and some [experiments](https://gitlab.com/mlockhart/lab/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=log%3A%3Aexperiment)

My lab book Project is a Public record, in accordance with GitLabs *Transparency* Value. The GitLab Value of *Efficiency* requests that we operate *handbook first*, but this should not be to the exclusion of maintaining one's own records. Some of these may be merged into official shared guides and projects. My purpose in recording notes in a dedicated project is that it is a [sound engineering practice](https://milosophical.me/blog/2015/4-bit-rules-of-computing-part-1.html).

Feel free to poke around. If you see something that needs correcting, or moving, then you are welcome to raise it:

* Make an [Issue](https://gitlab.com/mlockhart/lab/-/issues/new), or a [Merge Request](https://gitlab.com/mlockhart/lab/-/merge_requests/new)
* Open Slack and mention my handle: `@MikeL`

## Other Workbooks

For a quick way to review my work in other projects, check out:

* [my Assigned Issues](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&assignee_username=mlockhart)
* [my Merge Requests](https://gitlab.com/dashboard/merge_requests?scope=all&utf8=%E2%9C%93&state=all&assignee_username=mlockhart)
* [my Field notes](https://gitlab.com/gitlab-com/support/fieldnotes/-/issues/?sort=created_date&state=opened&assignee_username[]=mlockhart) (investigative notes for customer tickets)
* [my sub-project in gitlab-gold](https://gitlab.com/gitlab-gold/mlockhart-support) (for testing dotcom/ultimate features)
* [my Done To-Dos](https://gitlab.com/dashboard/todos?action_id=&author_id=&project_id=&state=done&type=)
* [my Someday Project](https://gitlab.com/mlockhart/someday) (for backlog Issues and ideas that are not active)

There is also a separate [Support Field notes](https://gitlab.com/gitlab-com/support/fieldnotes/) project for all GitLab Engineers. These often contain customers' sensitive information, so they remain Confidential to protect their privacy, but other GitLab team members are able to read them. Read about *Fieldnotes* in [it's Wiki page]((https://gitlab.com/mlockhart/lab/-/wikis/Fieldnotes)), or the [Fieldnote Issues Support Workflow](https://about.gitlab.com/handbook/support/workflows/fieldnote_issues.html) in the GitLab Handbook, the next *Iteration* of this idea.

Happy Hacking!
