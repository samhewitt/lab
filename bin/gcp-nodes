#!/usr/bin/env bash

# Stop/Start/List multiple Google Cloud Platform compute instances ("nodes")
#
# If run without arguments, will stop all nodes matching the patterns at end.
#
# Usage:
#  gcp-nodes [(start|stop|list|<other_action>) [<name_pattern> [<gcp_zone>]]]
#
# Default action is stop
# Default patterns are listed at the end of the script
# Default zone is us-west1-c
#
# originally in https://gitlab.com/-/snippets/2159476

# Note that to use the gcloud executable, you must be logged into Google Cloud:
#  $ gcloud auth login

# GCP project and Workspace
project_hash=56581c10
namespace=mlockhart

# These are so that gcloud will run from cron on an M1 Mac
export PATH=/opt/homebrew/bin:${PATH}
gcloud config set project ${namespace}-${project_hash}

function gcp_nodes(){
  local action=${1}
  local pattern=${2}
  local zone=${3}

  echo -e "\n\tAction: ${action}\tPattern: ${pattern}\tZone: ${zone}"

  if [[ ${action} == stop ]]; then
    status=RUNNING
  else
    status=TERMINATED
  fi

  if [[ ${action} == list ]]; then
    gcloud compute instances list --filter="(name${pattern})"
  else
    list=$( \
      gcloud compute instances list \
        --filter="(name${pattern} AND status=${status})" \
        --zones="${zone}" \
        --format="value[terminator=' '](name)")
    count=$(echo ${list} | awk -F' ' '{print NF}')
    echo "Found ${count} ${status} node(s) matching '${pattern}' in ${zone}"
    test ${count} -gt 0 && \
      gcloud compute instances ${action} ${list} --zone ${zone}
  fi
}

action=stop
[[ -n ${1} ]] && action=${1}

zone=us-west1-c
[[ -n ${3} ]] && zone=${3}

if [[ -n ${2} ]]; then
  gcp_nodes ${action} ~${2} ${zone}
else
  gcp_nodes ${action} ~-sr- ${zone}
  gcp_nodes ${action} ~-get-1- us-west1-c
  gcp_nodes ${action} ~-get-3- us-central1-c
  gcp_nodes ${action} ~-get-[4..9]- ${zone}
  gcp_nodes ${action} ~-ce- us-central1-c
  gcp_nodes ${action} !~mjl ${zone}
fi
echo Done
