# Defined in /Users/mjl/.config/fish/functions/gssh.fish @ line 1
function gssh --wraps='gcloud compute ssh' --description 'Open secure shell to a Google compute node'
  gcloud compute ssh $argv; 
end
