#!/usr/bin/env sh
export GITLAB_HOME=~/srv/gitlab-docker
sudo docker run -it \
  --hostname gitlab.example.com \
  --publish 8443:443 --publish 8080:80 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --shm-size 256m \
  gitlab/gitlab-ee:15.11.2-ee.0
