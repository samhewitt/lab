# Postgresql in a docker container, for learning

See https://gitlab.com/-/snippets/1970115#note_665435603

Boot up the database:

```sh
docker-compose up -d
```

Connect to the database:

```sh
docker-compose run psql
```

Re-build the environment (refresh `psqlrc`, or add a new package):

```sh
docker-compose up --build -d
```

Tear down:

```sh
docker-compose down
```
